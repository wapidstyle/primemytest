# PrimeMyTest
PrimeMyTest simply generates prime numbers.

## Usage
On first run:
```shell
npm install
```

Then, just run
```shell
node prime.js
```
